$.cssHooks.backgroundColor = {
    get: function(elem) {
        if (elem.currentStyle)
            var bg = elem.currentStyle["backgroundColor"];
        else if (window.getComputedStyle)
            var bg = document.defaultView.getComputedStyle(elem,
                null).getPropertyValue("background-color");
        if (bg.search("rgb") == -1)
            return bg;
        else {
            bg = bg.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            function hex(x) {
                return ("0" + parseInt(x).toString(16)).slice(-2);
            }
            return "#" + hex(bg[1]) + hex(bg[2]) + hex(bg[3]);
        }
    }
}

$(document).ready(function(){

	$('.units').addClass('hidden');
	$('#colours').removeClass('hidden');

	$('.styleguide').on({
		click: function(e) {
			e.preventDefault();
			var clickTarget = $(this).attr('href');
			$('.container.right .units').each(function(){
				$(this).addClass('hidden');
				if ($(this).attr('id') === clickTarget) {
					$(this).removeClass('hidden');
				}
			})
		}
	}, '.guide-navigation a');

	$('.colors div').each(function(){
		var bgcolor = $(this).css("backgroundColor");
		$(this).after('<span class="colorcode">' + bgcolor + '</span>');
	});

	$('#fontstacks p').each(function(){
		var fontstack = $(this).css('fontFamily');
		$(this).html(fontstack);
	});

});